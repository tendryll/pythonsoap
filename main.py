from zeep import Client
from zeep import Settings
from zeep import helpers


def list_city_names_type():
    with client.settings(raw_response=True):
        print('A list of latitude, longitude values associated with cities')
        response = client.service.LatLonListCityNames(1)
        _json = helpers.serialize_object(response, dict)
        print(_json)
        # for elem in _json['_raw_elements']:
        #     for item in elem:
        #         print(item)

        # print(_json['_raw_elements'])


def math():
    with client.settings(raw_response=False):
        print('Addition with soap. 1 + 3')
        response = client2.service.Add(1, 3)
        print(response)


if __name__ == '__main__':
    settings = Settings(strict=False, xml_huge_tree=True)
    client = Client(wsdl='https://graphical.weather.gov/xml/DWMLgen/wsdl/ndfdXML.wsdl', settings=settings)
    list_city_names_type()

    print('\n')
    client2 = Client(wsdl='http://www.dneonline.com/calculator.asmx?WSDL', settings=settings)
    client2.bind('Calculator', 'CalculatorSoap')
    math()
    # act(client)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
# reference: https://www.weather.gov/mdl/webservices_ndfd <- NOAA weather SOAP api
# reference: https://docs.python-zeep.org/en <- The Python SOAP client library documentation
